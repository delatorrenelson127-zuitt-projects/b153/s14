/*Instructions:

Create a function which will be able to add two numbers.
    -Numbers must be provided as arguments.
    -Display the result of the addition in our console.

Create a function which will be able to subtract two numbers.
    -Numbers must be provided as arguments.
    -Display the result of subtraction in our console.

Create function which will be able to multiply two numbers.
    -Numbers must be provided as arguments.
    -Return the result of the multiplication.

-Create a new variable called product.
    -This product should be able to receive the result of multiplication function.

Log the value of product variable in the console.

Pushing Instructions:

Go to Gitlab:
-in your zuitt-projects folder and access b153 folder.
-inside your b153 folder create a new folder/subgroup: s14
-inside s14, create a new project/repo called activity: a1
-untick the readme option
-copy the git url from the clone button of your activity repo.

Go to Gitbash:
-go to your b153/s14 folder and access activity folder
-initialize activity folder as a local repo: git init
-connect your local repo to our online repo: git remote add origin <gitURLOfOnlineRepo>
-add your updates to be committed: git add .
-commit your changes to be pushed: git commit -m "includes javascript intro activity"
-push your updates to your online repo: git push origin master

Go to Boodle:
-copy the url of the home page for your s14/activity repo (URL on browser not the URL from clone button) and link it to boodle:

WD078-14 | Javascript Introduction

*/

function addTwoNumbers(n1, n2) {
  console.log(n1 + n2);
}
addTwoNumbers(2, 2); // 4

function subtractTwoNumbers(n1, n2) {
  console.log(n1 - n2);
}
subtractTwoNumbers(5, 2); // 3

function multiplyTwoNumbers(n1, n2) {
  return n1 * n2;
}

let product = multiplyTwoNumbers(2, 3);

console.log(product); // 6
