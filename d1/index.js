console.log("Hello everyone!");

console.log("Nelson");
console.log("Sinigang na Bangus");

console.log("I end my statement without semi colon");

let variable1 = 3;
const CONSTANT1 = 3.14;

console.log(variable1);
console.log(CONSTANT1);

let role = "Manager";
let name2 = "Juan Dela Cruz";

role = "Director";
name2 = "Juan Dela Cruz";
const tin = 124444 - 1230;

console.log(name, " ", role, " ", tin);

// Mini Activity
let state = "California";
let country = "Philippines";
let country1 = "U.S.A";
let state1 = "Florida";
let province1 = "Rizal";
let city = "Tokyo";
let city1 = "Conhagen";
let country2 = "Japan";

let myCountry1 = province1 + ", " + country;
let myCountry2 = state + ", " + country1;
let myCountry3 = city + ", " + country2;

console.log(myCountry1);
console.log(myCountry2);
console.log(myCountry3);

let numString1 = "5";
let numString2 = "6";
let number1 = 10;
let number2 = 6;

console.log(numString1 + numString2); // "56"

console.log(number1 + number2); // 16

let number3 = 5.5;
let number4 = 0.5;
console.log(number1 + number4);
console.log(number3 + number4);

console.log(numString1 + number1); //510
console.log(number2 + numString2); // 66

// Boolean (true or false)
let isAdmin = true;
let isMarried = false;
let isMVP = true;

console.log("Is she married? " + isMarried);
console.log("Is Stephen Curry an MVP? " + isMVP);
console.log("Is he the current admin? " + isAdmin);

// Arrays
let array1 = ["Goku", "Gohan", "Vegeta", "Trunks", "Broly"];
let array2 = ["One Punch Man", "Saitama", true, 50000];
console.log(array1);
console.log(array2);

/* Objects
let obj = {
    key1: value1,
    key2,:value2
}
*/

let hero = {
  heroName: "One Punch Man",
  realName: "Saitama",
  powerLevel: 5000,
  isActive: true,
};

console.log(hero);
console.clear();

// Eraserheads
let mayFavoriteBand = [
  "Chito Miranda",
  "Vinci Montaner",
  "Gab Chee Kee",
  "Buwi Meneses",
  "Dindin Moreno",
  "Darius",
];

// console.log(mayFavoriteBand);

let user = {
  firstName: "Nelson",
  lastName: "Dela Torre",
  isDeveloper: true,
  age: 31,
};

// console.log(user);

// Undefined vs Null
/**
 * Null - is explicit absence of data/value. This is done to project that a variable contains nothing over undefined as merely mean ther is no data in the variable Because the variable has not been assigned to an initial value.
 */

let sampleNUll = null;
console.log(sampleNUll);
// undefined - is a representation that a variable has been declared but it was not assigned an initial value

let sampleUndefined;
console.log(sampleUndefined);

let foundResult = null;

let person2 = {
  name: "Peter",
  age: 35,
};

console.log(person2.isAdmin); // undefined
// 'undefined' because no isAdmin property in person2

/**
 * Functionin -  JS, are lines/blocks f codes that tell our application to perform a task when called/invoke
 *
 * Syntax
 * function functionName(){
 *     code block
 * }
 */

function greet(msg = "Hi", i) {
  for (let n = 0; n <= i; n++) {
    console.log(msg + "");
  }
}

greet("Hello", 10);
let n = "Joe";
function printName(n) {
  console.log(`My name is ${n}`);
}

console.log(n);
printName("Tan");

function displayMessage(msg) {
  console.log(msg);
}

displayMessage("Javascript is fun!");

// function can alse receive multiple parameter
function displayFullName(fn, mdn, ln) {
  console.log(`${fn} ${mdn} ${ln}`);
}

console.log("Peter", "Sanchez", "Parker");
